<?php


namespace App\Tests;


use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;


class UnitTest extends TestCase
{
    public function testVoiture()
    {
        $voiture = new Voiture();


        $voiture->setModele('polo');


        $this->assertTrue($voiture->getModele() === 'polo');
    }
}
