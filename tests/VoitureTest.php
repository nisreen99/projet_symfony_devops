<?php

namespace App\Tests;
use App\src\Controller\VoitureController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoitureTest extends WebTestCase
{
	public function testvoiture()
	{
		$client=static::createClient();
		$client->followRedirects();
		$crawler=$client->request('GET','/voiture');
		$this->assertResponseIsSuccessful();
		$this->assertSelectorTextContains('h1','Voiture index');
	}

	public function testcreateVoiture()
	{
		$client=static::createClient();
		$client->followRedirects();
		$crawler=$client->request('GET','/voiture/new');
		$this->assertResponseIsSuccessful();
		$h1Content= $crawler->filter('h1')->text();
		var_dump($h1Content);
		$this->assertSelectorTextContains('h1','Create new Voiture');
	}

}
