<?php

namespace App\Tests ;
use App\Entity\Voiture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTest  extends WebTestCase
{
public function testShouldDisplayVoitureIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Voiture index');
    }
}
